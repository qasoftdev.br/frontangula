const proxy = [
  {
    context: '/api',
    target: 'http://ec2-54-210-250-60.compute-1.amazonaws.com:8080',
    //  target: 'http://localhost:8080',
    pathRewrite: {'^/api' : ''}
  }
];
module.exports = proxy;
