import { Component, OnInit } from '@angular/core';
import { Usuario } from '../usuario.model';
import { UsuarioService } from '../usuario.service';
import { Router, ActivatedRoute, Route } from '@angular/router';

@Component({
  selector: 'app-usuario-deletar',
  templateUrl: './usuario-deletar.component.html',
  styleUrls: ['./usuario-deletar.component.css']
})
export class UsuarioDeletarComponent implements OnInit {

  usuario: Usuario

  constructor(private usuarioService: UsuarioService,private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {

    this.route.paramMap.subscribe(paramMap => {

      let id  = paramMap.get('id')

      this.usuarioService.pesquisarPorId(id).subscribe(usuario => {
        this.usuario = usuario;
      })
    })
  }

  deletaUsuario(): void {
    this.usuarioService.deletar(this.usuario.idUsuario).subscribe(() => {
      this.usuarioService.exibeMensagem('Usuario excluido com sucesso!')
      this.router.navigate(['/usuarios'])
    })
  }

  cancelar(): void {
    this.router.navigate(['/usuarios'])
  }

}
