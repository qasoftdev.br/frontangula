import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpClient } from '@angular/common/http';
import { Usuario } from './usuario.model';
import { Observable, EMPTY } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  /* backend no aws*/
  baseUrl = "http://ec2-54-210-250-60.compute-1.amazonaws.com:8080/api/usuario";

  /** Backend Local eclipse */
  //baseUrl = "http://localhost:8080/api/usuario";

  constructor( private snackBar: MatSnackBar, private http: HttpClient) { }

  exibeMensagem(msg: string, isError: boolean = false): void {
    this.snackBar.open(msg, 'X',{
      duration:3000,
      horizontalPosition: "right",
      verticalPosition: "top",
      panelClass: isError ? ['msg-error'] : ['msg-succes']
    })
  }

  cadastrar(usuario: Usuario): Observable<Usuario> {
    return this.http.post<Usuario>(this.baseUrl, usuario).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  listar(): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(this.baseUrl).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  pesquisarPorId(idUsuario: string): Observable<Usuario> {
    const url = `${this.baseUrl}/${idUsuario}`;
    return this.http.get<Usuario>(url).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );

  }

  atualizar(usuario: Usuario): Observable<Usuario> {

    const url = `${this.baseUrl}`;
     return this.http.post<Usuario>(url, usuario).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }
  /*put */
  // atualizar(usuario: Usuario): Observable<Usuario> {
  //   const url = `${this.baseUrl}/${usuario.idUsuario}`;

  //    return this.http.post<Usuario>(url, usuario).pipe(
  //     map((obj) => obj),
  //     catchError((e) => this.errorHandler(e))
  //   );
  // }

  deletar(idUsuario: number ): Observable<Usuario> {
    const url = `${this.baseUrl}/${idUsuario}`;
    return this.http.delete<Usuario>(url).pipe(
      map((obj) => obj),
      catchError((e) => this.errorHandler(e))
    );
  }

  errorHandler(e: any): Observable<any> {
    this.exibeMensagem('Ocorreu um erro!', true)
    return EMPTY
  }
}
