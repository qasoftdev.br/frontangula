export interface Usuario {

    idUsuario?: number;
    nome: string;
    cpf: string;
    email: string;
    whatsapp: string;
    admin: boolean;
    professor: boolean;
}
