import { Component, OnInit } from '@angular/core';
import { Usuario } from '../usuario.model';
import { UsuarioService } from '../usuario.service';

@Component({
  selector: 'app-usuario-listar',
  templateUrl: './usuario-listar.component.html',
  styleUrls: ['./usuario-listar.component.css']
})
export class UsuarioListarComponent implements OnInit {

  usuarios: Usuario[];
  displayedColumns = ['id','nome', 'cpf', 'email', 'whatsapp','acoes']

  constructor(private usuarioService: UsuarioService) { }

  ngOnInit(): void {
    this . usuarioService.listar().subscribe(usuarios => {
      this.usuarios = usuarios
      console.log(usuarios)
    })
  }

}
