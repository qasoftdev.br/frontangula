import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../usuario.service';
import { Router } from '@angular/router';
import { Usuario } from '../usuario.model';

@Component({
  selector: 'app-usuario-cadastro',
  templateUrl: './usuario-cadastro.component.html',
  styleUrls: ['./usuario-cadastro.component.css']
})
export class UsuarioCadastroComponent implements OnInit {

  usuario: Usuario = {

    nome: "",
    cpf: "",
    email: "",
    whatsapp: "",
    admin: false,
    professor: false
  }

  constructor(private usuarioService: UsuarioService, private router: Router) { }

  ngOnInit(): void {

  }

  cadastroProduto(): void  {
    this.usuarioService.cadastrar(this.usuario).subscribe(()=> {
      this.usuarioService.exibeMensagem('Usuário criado!')
      this.router.navigate(['/usuarios'])
    })

  }

  cancelar():void  {
    this.router.navigate(['/usuarios'])
  }

}
