import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UsuarioService } from '../usuario.service';
import { Usuario } from '../usuario.model';

@Component({
  selector: 'app-usuario-atualizar',
  templateUrl: './usuario-atualizar.component.html',
  styleUrls: ['./usuario-atualizar.component.css']
})
export class UsuarioAtualizarComponent implements OnInit {

  usuario: Usuario

  constructor(private usuarioService: UsuarioService,
    private router:  Router, private route: ActivatedRoute) { }

  ngOnInit(): void {

    this.route.paramMap.subscribe(paramMap => {
      let id  = paramMap.get('id')

      if (id!= null) {
        console.log('Teste de id  :' + id + 'localizado')

        this.usuarioService.pesquisarPorId(id).subscribe(usuario => {
          this.usuario = usuario
          console.log( 'trouxe o usuario no objeto:'+ usuario.nome)
        })
      } else {
        console.log('ERRO! NÃO ACHOU O ID  :' + this.usuario)
        this.usuarioService.exibeMensagem('Usuário não localizado!')
      }

    })



  }

  atualizaUsuario(): void {
    this.usuarioService.atualizar(this.usuario).subscribe(() => {
      this.usuarioService.exibeMensagem('Usuário atualizado com sucesso!')
      this.router.navigate(['/usuarios'])
    })
  }

  cancelar(): void {
    this.router.navigate(['/usuarios'])
  }

}
