import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HeaderService } from '../../template/header/header.service';

@Component({
  selector: 'app-usuario-crud',
  templateUrl: './usuario-crud.component.html',
  styleUrls: ['./usuario-crud.component.css']
})
export class UsuarioCrudComponent implements OnInit {

  constructor(private router: Router, private headService: HeaderService) {
    headService.headerData = {
      title: 'Cadastro de Usuários',
      icon: 'people',
      routeUrl: '/usuarios'
    }
  }

  ngOnInit(): void {
  }


  navigateToUsuarioCadastro(): void {
    this.router.navigate(['/usuarios/cadastro'])
  }

}
