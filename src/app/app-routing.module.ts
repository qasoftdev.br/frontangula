import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/views/home/home.component';
import { UsuarioCrudComponent } from './components/views/usuario-crud/usuario-crud.component';
import { UsuarioCadastroComponent } from './components/usuario/usuario-cadastro/usuario-cadastro.component';
import { UsuarioAtualizarComponent } from './components/usuario/usuario-atualizar/usuario-atualizar.component';
import { UsuarioDeletarComponent } from './components/usuario/usuario-deletar/usuario-deletar.component';

const routes: Routes = [{
  path:"",
  component: HomeComponent
},
{
  path: "usuarios",
  component: UsuarioCrudComponent
},
{
  path: "usuarios/cadastro",
  component: UsuarioCadastroComponent
},
{
  path: "usuarios/atualizar/:id",
  component: UsuarioAtualizarComponent
},
{
  path: "usuarios/deletar/:id",
  component: UsuarioDeletarComponent
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],


exports: [RouterModule]
})
export class AppRoutingModule { }
